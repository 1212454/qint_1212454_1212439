#include <deque>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
class  QInt
{
public:
	void SetValue(string);
	string ToString();

	
	
	void Output();
	void Clear();
	void ReadFile(string);

	// Bin-Hex
	const char* HexCharToBin(char);
	string HexToBin(string);
	string BinToHex();

	// Bin-Dec
	string DecToBin(string);
	string BinToDec();
	
	

	// DecHelpers
	string Sum(string, string);
	string Pow2(int);
	string Div2(string);
	string Mult2(string);
private:
	deque<char> bits;
};

 void QInt::SetValue(string input)
{
	if (!bits.empty())
	{
		bits.clear();
	}
	for (int i = 0; i < input.length(); i++)
	{
		bits.push_back(input[i]);
	}
}

 string QInt::ToString()
 {
	 string str;
	 for (int i = 0; i < bits.size(); i++)
	 {
		 str = bits[i] + str;
	 }
	 return str;
 }


 string QInt::Div2(string num)
 {
	 string kq;
	 int tmp;
	 int exc = 0;
	 int len = num.length();
	 for (int i = 0; i < len; i++)
	 {
		 tmp = num[i] - 48 + exc * 10;
		 if (tmp != 1)
		 {
			 kq = kq + (char)((tmp / 2) + 48);
			 exc = tmp % 2;
		 }
		 else
		 {
			 kq = kq + "0";
			 exc = tmp % 2;
		 }
	 }
	 if (kq[0] == '0' && kq.length() > 1)
	 {
		 kq = kq.substr(1, (kq.length() - 1));
	 }
	 return kq;
 }

 string QInt::Mult2(string x)
 {
	 string kq;
	 int tmp;
	 int flag = 0;
	 for (int i = x.length() - 1; i >= 0; i--)
	 {

		 tmp = ((x[i] - 48) * 2) + flag;
		 flag = tmp / 10;;
		 tmp = tmp % 10;
		 kq = char(tmp + 48) + kq;
	 }
	 if (flag == 1)
	 {
		 kq = "1" + kq;
	 }
	 return kq;
 }



 string QInt::Sum(string a, string b)
 {
	 string kq;
	 int tmp;
	 int flag = 0;
	 char g;
	 while (a.length() > b.length())
	 {
		 b = "0" + b;
	 }
	 while (a.length() < b.length())
	 {
		 a = "0" + a;
	 }
	 for (int i = a.length() - 1; i >= 0; i--)
	 {
		 tmp = (a[i] - 48) + (b[i] - 48) + flag;
		 if (tmp > 9)
		 {
			 tmp = tmp - 10;
			 flag = 1;
		 }
		 else flag = 0;

		 kq = (char)(tmp + 48) + kq;
	 }
	 if (flag == 1)
		 kq = "1" + kq;
	 return kq;
 }


 string QInt::Pow2(int n)
 {
	 string kq = "1";
	 for (int i = 0; i < n; i++)
	 {
		 kq = Mult2(kq);
	 }
	 return kq;
 }

 string QInt::DecToBin(string dec)
 {
	 int tmp;
	 while (dec != "0")
	 {
		 tmp = dec[dec.length() - 1] - 48;
		 bits.push_front((tmp % 2) + 48);
		 dec = Div2(dec);
	 }
	 for (int i = bits.size(); i < 128; i++)
	 {
		 bits.push_front('0');
	 }
	 string binStr = ToString();
	 return binStr;
 }

 string QInt::BinToDec()
 {
	 string dec = "0";
	 for (int i = 1; i <= bits.size(); i++)
	 {
		 if ((bits[bits.size() - i]) == '1')
		 {
			 dec = Sum(dec, Pow2(i));
		 }
	 }
	 return dec;
 }



string QInt::BinToHex()
{
	string kq;
	string temp;
	deque<char> hex;
	int i;
	int decVal;
	while((bits.size() % 4) != 0)
	{
		bits.push_front('0');
	}
	for (int i = 0; i < bits.size(); i+=4)
	{
		temp = temp + bits[i] + bits[i + 1] + bits[i + 2] + bits[i + 3];
		decVal = (bits[i] - 48) *8 + (bits[i + 1] - 48) * 4 + (bits[i + 2] - 48) * 2 + (bits[i + 3] - 48) * 1;
		switch ( decVal)
		{
		case 0:
			kq += "0"; break;
		case 1:
			kq += "1"; break;
		case 2:
			kq += "2"; break;
		case 3:
			kq += "3"; break;
		case 4:
			kq += "4"; break;
		case 5:
			kq += "5"; break;
		case 6:
			kq += "6"; break;
		case 7:
			kq += "7"; break;
		case 8:
			kq += "8"; break;
		case 9:
			kq += "9"; break;
		case 10:
			kq += "A"; break;
		case 11:
			kq += "B"; break;
		case 12:
			kq += "C"; break;
		case 13:
			kq += "D"; break;
		case 14:
			kq += "E"; break;
		case 15:
			kq += "F"; break;
		default:
			break;
		}
	}
	return kq;
}

const char* QInt::HexCharToBin(char c)
{
	switch (toupper(c))
	{
	case '0':
		return "0000";
	case '1': 
		return "0001";
	case '2': 
		return "0010";
	case '3': 
		return "0011";
	case '4': 
		return "0100";
	case '5': 
		return "0101";
	case '6': 
		return "0110";
	case '7': 
		return "0111";
	case '8': 
		return "1000";
	case '9': 
		return "1001";
	case 'A': 
		return "1010";
	case 'B': 
		return "1011";
	case 'C': 
		return "1100";
	case 'D': 
		return "1101";
	case 'E': 
		return "1110";
	case 'F': 
		return "1111";
	}
}

string QInt::HexToBin(string hex)
{
	string bin;
	for (int i = 0; i < hex.size(); i++)
		bin += HexCharToBin(hex[i]);
	for (int i = bin.length(); i < 128; i++)
	{
		bin = "0" + bin;
	}
	SetValue(bin);
	return bin;
}

void QInt::Output()
{
	for (int i = 0; i < bits.size(); i++)
		cout << bits[i];
	cout << endl;
}

void QInt::Clear()
{
	bits.clear();
}

void ReadFile(string nameFile)
{
	string buffer;
	fstream file;
	string num, num2;
	QInt qint;
	string temp;
	string kq;
	file.open(nameFile, ios::in);
	if (!file)
	{
		cout << "Khong the mo file!!!" << endl;
		exit(0);
		file.close();
	}
	while (getline(file, buffer))
	{
		num = buffer.substr(0, buffer.find(" "));
		buffer.erase(0, buffer.find(" ") + 1);
		

	}
	
	file.close();
	file.open("output.txt", ios::out);
	file << kq;
	file.close();
}
